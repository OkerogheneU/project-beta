import React, { useState, useEffect } from 'react';

const RecordANewSale = () => {
  const [formData, setFormData] = useState({
    automobile: '',
    salesperson: '',
    customer: '',
    price: '',
  });

  const [unsoldAutomobiles, setUnsoldAutomobiles] = useState([]);

  useEffect(() => {
    const fetchUnsoldAutomobiles = async () => {
      try {
        const response = await fetch('/api/unsold-automobiles');
        const data = await response.json();
        setUnsoldAutomobiles(data);
      } catch (error) {
        console.error('Error fetching unsold automobiles:', error);
      }
    };

    fetchUnsoldAutomobiles();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const url = 'http://localhost:8090/api/sales/';

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(url, fetchConfig);

      if (response.ok) {
        setFormData({
          automobile: '',
          salesperson: '',
          customer: '',
          price: '',
        });
        console.log('Sale recorded successfully');
      } else {
        console.error('Error recording sale:', response.statusText);
      }
    } catch (error) {
      console.error('Error recording sale:', error);
    }
  };

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a New Sale</h1>
          <form onSubmit={handleSubmit} id="add-sale-form">
            <div className="mb-3">
              <label htmlFor="automobile" className="form-label">
                Automobile VIN:
              </label>
              <select
                name="automobile"
                onChange={handleChange}
                value={formData.automobile}
                required
                className="form-select"
              >
                <option value="" disabled>Select an Automobile</option>
                {unsoldAutomobiles.map((automobile) => (
                  <option key={automobile.id} value={automobile.id}>
                    {automobile.name}
                  </option>
                ))}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="salesperson" className="form-label">
                Salesperson:
              </label>
              <input
                type="text"
                name="salesperson"
                placeholder="Enter Salesperson Name"
                onChange={handleChange}
                value={formData.salesperson}
                required
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="customer" className="form-label">
                Customer:
              </label>
              <input
                type="text"
                name="customer"
                placeholder="Enter Customer Name"
                onChange={handleChange}
                value={formData.customer}
                required
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="price" className="form-label">
                Price:
              </label>
              <input
                type="number"
                name="price"
                placeholder="Enter Sale Price"
                onChange={handleChange}
                value={formData.price}
                required
                className="form-control"
              />
            </div>

            <button type="submit" className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default RecordANewSale;
