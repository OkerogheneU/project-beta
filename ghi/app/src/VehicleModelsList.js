
import React, { useState, useEffect } from 'react';

const VehicleModelsList = () => {
  const [vehicleModels, setVehicleModels] = useState([]);

  useEffect(() => {
    const fetchVehicleModels = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/models/');
        const data = await response.json();
        setVehicleModels(data.models);
      } catch (error) {
        console.error('Error fetching vehicle models:', error);
      }
    };

    fetchVehicleModels();
  }, []);

  return (
    <div>
      <h2>List of Vehicle Models</h2>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {vehicleModels.map(vehicleModel => (
            <tr key={vehicleModel.id}>
              <td>{vehicleModel.name}</td>
              <td>{vehicleModel.manufacturer.name}</td>
              <td>
                <img src={vehicleModel.picture_url} alt={vehicleModel.name} style={{ width: '100px' }} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default VehicleModelsList;
