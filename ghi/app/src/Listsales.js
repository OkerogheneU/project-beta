import React, { useState, useEffect } from 'react';

const SalesList = () => {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    const fetchSales = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();

        if (typeof data.data === 'string') {

          const parsedData = JSON.parse(data.data);
          setSales(parsedData);
        } else if (Array.isArray(data)) {

          setSales(data);
        } else {
          console.error('Invalid data format:', data);
        }
      } catch (error) {
        console.error('Error fetching sales data:', error);
      }
    };

    fetchSales();
  }, []);

  return (
    <div>
      <div style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
        <h2 style={{ marginRight: '10px' }}>Sales</h2>
      </div>
      <table>
        <thead>
          <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => (
            <tr key={sale.id}>
              <td>{sale.salesperson.employee_id}</td>
              <td>{sale.salesperson.name}</td>
              <td>{sale.customer}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default SalesList;



// import React, { useState, useEffect } from 'react';

// const SalesList = () => {
//   const [sales, setSales] = useState([]);

//   useEffect(() => {
//     const fetchSales = async () => {
//       try {
//         const response = await fetch('http://localhost:8090/api/sales/');
//         if (!response.ok) {
//           throw new Error(`HTTP error! Status: ${response.status}`);
//         }

//         const data = await response.json();

//         if (Array.isArray(data)) {
//           setSales(data);
//         } else {
//           console.error('Invalid data format:', data);
//         }
//       } catch (error) {
//         console.error('Error fetching sales data:', error);
//       }
//     };

//     fetchSales();
//   }, []);

//   return (
//     <div>
//       <div style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
//         <h2 style={{ marginRight: '10px' }}>Sales</h2>
//       </div>
//       <table>
//         <thead>
//           <tr>
//             <th>Salesperson Employee ID</th>
//             <th>Salesperson Name</th>
//             <th>Customer</th>
//             <th>VIN</th>
//             <th>Price</th>
//           </tr>
//         </thead>
//         <tbody>
//           {sales.map((sale) => (
//             <tr key={sale.id}>
//               <td>{sale.salesperson.employee_id}</td>
//               <td>{sale.salesperson.name}</td>
//               <td>{sale.customer}</td>
//               <td>{sale.automobile.vin}</td>
//               <td>{sale.price}</td>
//             </tr>
//           ))}
//         </tbody>
//       </table>
//     </div>
//   );
// };

// export default SalesList;



// // import React, { useState, useEffect } from 'react';

// // const SalesList = () => {
// //   const [sales, setSales] = useState([]);

// //   useEffect(() => {
// //     const fetchSales = async () => {
// //       try {
// //         const response = await fetch('http://localhost:8090/api/sales/');
// //         if (!response.ok) {
// //           throw new Error(`HTTP error! Status: ${response.status}`);
// //         }

// //         const data = await response.json();
// //         if (data && data.data && Array.isArray(data.data)) {
// //           setSales(data.data);
// //         } else {
// //           console.error('Invalid data format:', data);
// //         }
// //       } catch (error) {
// //         console.error('Error fetching sales data:', error);
// //       }
// //     };

// //     fetchSales();
// //   }, []);

// //   return (
// //     <div>
// //       <div style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
// //         <h2 style={{ marginRight: '10px' }}>Sales</h2>
// //       </div>
// //       <table>
// //         <thead>
// //           <tr>
// //             <th>Salesperson Employee ID</th>
// //             <th>Salesperson Name</th>
// //             <th>Customer</th>
// //             <th>VIN</th>
// //             <th>Price</th>
// //           </tr>
// //         </thead>
// //         <tbody>
// //           {sales.map((sale) => (
// //             <tr key={sale.id}>
// //               <td>{sale.salesperson.employee_id}</td>
// //               <td>{sale.salesperson.name}</td>
// //               <td>{sale.customer}</td>
// //               <td>{sale.automobile.vin}</td>
// //               <td>{sale.price}</td>
// //             </tr>
// //           ))}
// //         </tbody>
// //       </table>
// //     </div>
// //   );
// // };

// // export default SalesList;
