import React, { useState, useEffect } from 'react';

function CreateAppointment() {
  const [appointment, setAppointment] = useState({
    vin: '',
    customer: '',
    date_time: '',
    reason: '',
    technician: '',
    status: '',
  });
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8080/api/technicians/')
      .then(response => response.json())
      .then(data => setTechnicians(data))
      .catch(error => console.error('Error fetching technicians:', error));
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await fetch('http://localhost:8080/api/appointments/', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(appointment),
      });
      if (!response.ok) throw new Error('Failed to create appointment');
      alert('Appointment created successfully');
      setAppointment({ vin: '', customer: '', date_time: '', reason: '', technician: '', status: '' });
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label>VIN:</label>
        <input type="text" value={appointment.vin} onChange={e => setAppointment({ ...appointment, vin: e.target.value })} />
      </div>
      <div>
        <label>Customer Name:</label>
        <input type="text" value={appointment.customer} onChange={e => setAppointment({ ...appointment, customer: e.target.value })} />
      </div>
      <div>
        <label>Date and Time:</label>
        <input type="datetime-local" value={appointment.date_time} onChange={e => setAppointment({ ...appointment, date_time: e.target.value })} />
      </div>
      <div>
        <label>Reason:</label>
        <input type="text" value={appointment.reason} onChange={e => setAppointment({ ...appointment, reason: e.target.value })} />
      </div>
      <div>
        <label>Technician:</label>
        <select value={appointment.technician} onChange={e => setAppointment({ ...appointment, technician: e.target.value })}>
          <option value="">Select a Technician</option>
          {technicians.map(tech => (
            <option key={tech.id} value={tech.id}>{tech.first_name} {tech.last_name}</option> // Assuming first_name and last_name are correct
          ))}
        </select>
      </div>
      <div>
        <label>Status:</label>
        <select value={appointment.status} onChange={e => setAppointment({ ...appointment, status: e.target.value })}>
          <option value="">Select Status</option>
          <option value="Scheduled">Scheduled</option>
          <option value="Completed">Completed</option>
          <option value="Cancelled">Cancelled</option>
        </select>
      </div>
      <button type="submit">Create Appointment</button>
    </form>
  );
}

export default CreateAppointment;
