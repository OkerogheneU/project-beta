import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import CreateManufacturer from './CreateManufacturer';
import VehicleModelsList from './VehicleModelsList';
import CreateVehicleModel from './CreateVehicleModel';
import AutomobilesList from './AutomobilesList';
import CreateAutomobile from './CreateAutomobile';
import AddTechnician from './AddTechnician';
import ListTechnicians from './ListTechnicians';
import CreateAppointment from './CreateAppointment';
import ListAppointments from './ListAppointments';
import ServiceHistory from './ServiceHistory';
import AddSalesperson from './AddSalesperson';
import Salespeople from './Salespeople';
import AddCustomer from './AddCustomer';
import Recordsale from './Recordsale';
import SalesList from './Listsales';
import SalespersonHistory from './SalesPersonHistory';
import ListCustomer from './ListCustomer';
import './index.css';


function App() {
  return (
    <BrowserRouter>
      <div className="container">
        <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/create-manufacturer" element={<CreateManufacturer />} />
          <Route path="/vehicle-models" element={<VehicleModelsList />} />
          <Route path="/create-vehicle-model" element={<CreateVehicleModel />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/create-automobile" element={<CreateAutomobile />} />
          <Route path="/add-technician" element={<AddTechnician />} />
          <Route path="/technicians" element={<ListTechnicians />} />
          <Route path="/create-appointment" element={<CreateAppointment />} />
          <Route path="/appointments" element={<ListAppointments />} />
          <Route path="/service-history" element={<ServiceHistory />} />
          <Route path="/add-salesperson" element={<AddSalesperson />} />
          <Route path="/list-salespeople" element={<Salespeople />} />
          <Route path="/add-customer" element={<AddCustomer />} />
          <Route path="/record-sale" element={<Recordsale />} />
          <Route path="/sales-list" element={<SalesList />} />
          <Route path="/sales-person-history" element={<SalespersonHistory />} />
          <Route path="/list-customers" element={<ListCustomer />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
