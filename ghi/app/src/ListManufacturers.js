import React, { useState, useEffect } from 'react';

function ListManufacturers() {
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8100/api/manufacturers/')
      .then(response => response.json())
      .then(data => setManufacturers(data.manufacturers))
      .catch(error => console.error('Error fetching manufacturers:', error));
  }, []);

  return (
    <div>
      <h2>Manufacturers</h2>
      <ul>
        {manufacturers.map(manufacturer => (
          <li key={manufacturer.id}>{manufacturer.name}</li>
        ))}
      </ul>
    </div>
  );
}

export default ListManufacturers;
