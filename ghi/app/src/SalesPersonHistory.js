import React, { useState, useEffect } from 'react';

const SalespersonHistory = () => {
  const [salespeople, setSalespeople] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [salesHistory, setSalesHistory] = useState([]);

  useEffect(() => {

    const fetchSalespeople = async () => {
      try {
        const response = await fetch('/api/salespeople');
        const data = await response.json();
        setSalespeople(data);
      } catch (error) {
        console.error('Error fetching salespeople:', error);
      }
    };

    fetchSalespeople();
  }, []);

  const handleSalespersonChange = async (e) => {
    const selectedSalespersonId = e.target.value;


    const url = `/api/saleshistory?salespersonId=${selectedSalespersonId}`;

    try {
      const response = await fetch(url);
      const data = await response.json();
      setSalesHistory(data);
      setSelectedSalesperson(selectedSalespersonId);
    } catch (error) {
      console.error('Error fetching sales history:', error);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Salesperson History</h1>
          <div className="mb-3">
            <label htmlFor="salesperson" className="form-label">
              Select Salesperson:
            </label>
            <select
              name="salesperson"
              onChange={handleSalespersonChange}
              value={selectedSalesperson}
              className="form-select"
            >
              <option value="" disabled>Select a Salesperson</option>
              {salespeople.map((salesperson) => (
                <option key={salesperson.id} value={salesperson.id}>
                  {salesperson.name}
                </option>
              ))}
            </select>
          </div>

          {salesHistory.length > 0 && (
  <div>
    <h2>Sales History for {selectedSalesperson}</h2>
    <table className="table">
      <thead>
        <tr>
          <th>Salesperson</th>
          <th>Customer</th>
          <th>Automobile VIN</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {salesHistory.map((sale) => (
          <tr key={sale.id}>
            <td>{sale.salesperson}</td>
            <td>{sale.customer}</td>
            <td>{sale.automobile}</td>
            <td>{sale.price}</td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
)}

{salesHistory.length === 0 && (
  <p>No sales history available for {selectedSalesperson}</p>
)}

        </div>
      </div>
    </div>
  );
};

export default SalespersonHistory;
