import React, { useState } from 'react';

const CreateAutomobile = () => {
  const [formData, setFormData] = useState({
    color: '',
    year: '',
    vin: '',
    model_id: '',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(url, fetchConfig);

      if (response.ok) {
        setFormData({
          color: '',
          year: '',
          vin: '',
          model_id: '',
        });
        console.log('Automobile created successfully');
      } else {
        console.error('Error creating automobile:', response.statusText);
      }
    } catch (error) {
      console.error('Error creating automobile:', error);
    }
  };

  const handleFormChange = (event) => {
    const { name, value } = event.target;
    setFormData((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add Automobile</h1>
          <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
                value={formData.color}
              />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Year"
                required
                type="number"
                name="year"
                id="year"
                className="form-control"
                value={formData.year}
              />
              <label htmlFor="year">Year</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="VIN"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
                value={formData.vin}
              />
              <label htmlFor="vin">VIN</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Model ID"
                required
                type="number"
                name="model_id"
                id="model_id"
                className="form-control"
                value={formData.model_id}
              />
              <label htmlFor="model_id">Model ID</label>
            </div>

            <button className="btn btn-primary" type="submit">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateAutomobile;
