import React, { useState } from 'react';

const AddCustomer = () => {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    address: '',
    phone_number: '',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();


    const url = 'http://localhost:8090/api/customers/';

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {

      setFormData({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Customer</h1>
          <form onSubmit={handleSubmit} id="add-customer-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="First Name"
                required
                type="text"
                name="first_name"
                id="firstname"
                className="form-control"
                value={formData.first_name}
              />
              <label htmlFor="firstname">First Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Last Name"
                required
                type="text"
                name="last_name"
                id="lastname"
                className="form-control"
                value={formData.last_name}
              />
              <label htmlFor="lastname">Last Name</label>
            </div>

            <div className="form-floating mb-3">
              <textarea
                onChange={handleFormChange}
                placeholder="Address"
                required
                name="address"
                id="address"
                className="form-control"
                value={formData.address}
              ></textarea>
              <label htmlFor="address">Address</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Phone Number"
                required
                type="text"
                name="phone_number"
                id="phoneNumber"
                className="form-control"
                value={formData.phone_number}
              />
              <label htmlFor="phoneNumber">Phone Number</label>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AddCustomer;
