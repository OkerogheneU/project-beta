import React, { useState, useEffect } from 'react';

function CustomersList() {
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    const url = 'http://localhost:8090/api/customers/';

    const fetchCustomers = async () => {
      try {
        const response = await fetch(url);
        const responseData = await response.json();

        if (Array.isArray(responseData.data)) {
          setCustomers(responseData.data);
        } else {
          console.error('Invalid data format:', responseData);
        }
      } catch (error) {
        console.error('Error fetching customers:', error);
      }
    };

    fetchCustomers();
  }, []);

  return (
    <div>
      <h1>Customers</h1>
      <table className="table">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>Phone number</th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer) => (
            <tr key={customer.id}>
              <td>{customer.first_name || 'First Name Placeholder'}</td>
              <td>{customer.last_name || 'Last Name Placeholder'}</td>
              <td>{customer.address}</td>
              <td>{customer.phone_number}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default CustomersList;
