import React, { useState, useEffect } from 'react';

const AutomobilesList = () => {
  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    const fetchAutomobiles = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        const data = await response.json();
        setAutomobiles(data.autos);
      } catch (error) {
        console.error('Error fetching automobiles:', error);
      }
    };

    fetchAutomobiles();
  }, []);

  return (
    <div>
      <h2>List of Automobiles in Inventory</h2>
      <table>
        <thead>
          <tr>
            <th>VIN</th>
            <th>Year</th>
            <th>Sold</th>
            <th>Color</th>
            <th>Model</th>
            <th>Manufacturer</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((automobile) => (
            <tr key={automobile.id}>
              <td>{automobile.vin}</td>
              <td>{automobile.year}</td>
              <td>{automobile.sold ? 'Yes' : 'No'}</td>
              <td>{automobile.color}</td>
              <td>{automobile.model.name}</td>
              <td>{automobile.model.manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default AutomobilesList;
