import React, { useEffect, useState } from 'react';
import AddTechnician from './AddTechnician';

function ListTechnicians() {
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    const fetchTechnicians = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (!response.ok) throw new Error('Network response was not ok.');
        const data = await response.json();
        setTechnicians(data);
      } catch (error) {
        console.error('There has been a problem with your fetch operation:', error);
      }
    };
    fetchTechnicians();
  }, []);

  return (
    <div>
      <h2>Technicians List</h2>
      {technicians.map(technician => (
        <div key={technician.id}>
          {technician.first_name} {technician.last_name}
        </div>
      ))}
    </div>
  );

}

export default ListTechnicians;
