import React, { useEffect, useState } from 'react';

const Salespeople = () => {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    const fetchSalespeople = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        const data = await response.json();
        setSalespeople(data.data);
      } catch (error) {
        console.error('Error fetching salespeople:', error);
      }
    };

    fetchSalespeople();
  }, []);

  return (
    <div>
      <h1>Salespeople</h1>
      <table className="table">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map(salesperson => (
            <tr key={salesperson.id}>
              <td>{salesperson.employee_id || 'N/A'}</td>
              <td>{salesperson.first_name || 'First Name Placeholder'}</td>
              <td>{salesperson.last_name || 'Last Name Placeholder'}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Salespeople;
