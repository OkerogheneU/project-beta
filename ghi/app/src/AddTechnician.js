import React, { useState } from 'react';

const AddTechnician = () => {
  const [technician, setTechnician] = useState({
    first_name: '',
    last_name: '',
    employee_id: ''
  });

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await fetch('http://localhost:8080/api/technicians/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(technician),
      });
      if (!response.ok) {
        throw new Error('Network response was not ok.');
      }
      alert('Technician added!');
      setTechnician({
        first_name: '',
        last_name: '',
        employee_id: ''
      });
    } catch (error) {
      console.error('There has been a problem with your fetch operation:', error);
    }
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setTechnician(prevTechnician => ({
      ...prevTechnician,
      [name]: value
    }));
  };

  return (

    <form onSubmit={handleSubmit}>
      <label>
        First Name:
        <input
          type="text"
          name="first_name"
          value={technician.first_name}
          onChange={handleChange}
        />
      </label>
      <label>
        Last Name:
        <input
          type="text"
          name="last_name"
          value={technician.last_name}
          onChange={handleChange}
        />
      </label>
      <label>
        Employee ID:
        <input
          type="text"
          name="employee_id"
          value={technician.employee_id}
          onChange={handleChange}
        />
      </label>
      <button type="submit">Submit</button>
    </form>
  );
};

export default AddTechnician;
