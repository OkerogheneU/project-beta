import React, { useEffect, useState } from 'react';

function ListAppointments() {
  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    fetchAppointments();
  }, []);

  const fetchAppointments = () => {
    fetch('http://localhost:8080/api/appointments/')
      .then(response => response.json())
      .then(data => setAppointments(data))
      .catch(error => console.error('Error fetching appointments:', error));
  };

  const updateAppointmentStatus = (id, newStatus) => {
    const statusMapping = {
      'Completed': 'finished',
      'Cancelled': 'canceled'
    };

    const backendStatus = statusMapping[newStatus];
    if (!backendStatus) {
      console.error('Invalid status:', newStatus);
      return; // Early return if status is not recognized
    }

    // Ensure the correct URL format and method are used
    fetch(`http://localhost:8080/api/appointments/${id}/status/`, { // Note the trailing slash
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ status: backendStatus }),
    })
    .then(response => {
      if (!response.ok) {
        // Handle non-OK responses here
        throw new Error(`Failed to update appointment status: ${response.statusText}`);
      }
      // If successful, filter out the appointment from the list
      setAppointments(prevAppointments => prevAppointments.filter(appointment => appointment.id !== id));
    })
    .catch(error => {
      // Proper error handling
      console.error('Error updating appointment status:', error);
    });
  };





  return (
    <div>
      <h2>Appointments</h2>
      <ul>
        {appointments.map(app => (
          <li key={app.id}>
            {app.date_time} - {app.customer} ({app.vin}) - {app.reason} - Technician ID: {app.technician}
            - Status: {app.status}
            <button onClick={() => updateAppointmentStatus(app.id, 'Completed')}>Complete</button>
            <button onClick={() => updateAppointmentStatus(app.id, 'Cancelled')}>Cancel</button>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default ListAppointments;
