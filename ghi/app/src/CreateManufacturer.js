import React, { useState } from 'react';

const CreateManufacturer = () => {
  const [newManufacturer, setNewManufacturer] = useState({
    name: '',
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewManufacturer((prevManufacturer) => ({
      ...prevManufacturer,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newManufacturer),
      });

      if (response.ok) {

        setNewManufacturer({ name: '' });

        alert('Manufacturer added!');
      } else {

        console.error('Failed to create manufacturer with response status:', response.status);
      }

      const data = await response.json();
      console.log('Manufacturer created:', data);
    } catch (error) {
      console.error('Error creating manufacturer:', error);
    }
  };

  return (
    <div>
      <h2>Create a Manufacturer</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Manufacturer Name:</label>
          <input
            type="text"
            id="name"
            name="name"
            value={newManufacturer.name}
            onChange={handleInputChange}
          />
        </div>
        <button type="submit">Create Manufacturer</button>
      </form>
    </div>
  );
};

export default CreateManufacturer;
