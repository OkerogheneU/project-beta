import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="d-flex flex-wrap">
            <NavLink className="nav-link custom-link" to="/manufacturers">ManufacturersList</NavLink>
            <NavLink className="nav-link custom-link" to="/create-manufacturer">CreateManufacturer</NavLink>
            <NavLink className="nav-link custom-link" to="/vehicle-models">VehicleModelsList</NavLink>
            <NavLink className="nav-link custom-link" to="/automobiles">AutomobilesList</NavLink>
            <NavLink className="nav-link custom-link" to="/create-automobile">CreateAutomobile</NavLink>
            <NavLink className="nav-link custom-link" to="/add-salesperson">Add Salesperson</NavLink>
            <NavLink className="nav-link custom-link" to="/list-salespeople">Salespeople</NavLink>
            <NavLink className="nav-link custom-link" to="/add-customer">Add Customer</NavLink>
            <NavLink className="nav-link custom-link" to="/list-customers">List Customers</NavLink>
            <NavLink className="nav-link custom-link" to="/record-sale">Record Sale</NavLink>
            <NavLink className="nav-link custom-link" to="/sales-list">SalesList</NavLink>
            <NavLink className="nav-link custom-link" to="/sales-person-history">SalesPersonHistory</NavLink>
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink className="nav-link" to="/create-vehicle-model">CreateVehicleModel</NavLink>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  More
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" to="/add-technician">Add Technician</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/technicians">Technicians</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/create-appointment">Create Appointment</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/appointments">Appointments</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/service-history">Service History</NavLink></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
