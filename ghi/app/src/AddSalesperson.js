import React, { useState } from 'react';

const SalespersonForm = () => {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id: '',
  });

  const handleSubmit = async (event) => {
    event.preventDefault();


    const url = 'http://localhost:8090/api/salespeople/';

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {

      setFormData({
        first_name: '',
        last_name: '',
        employee_id: '',
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Salesperson</h1>
          <form onSubmit={handleSubmit} id="add-salesperson-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="First Name"
                required
                type="text"
                name="first_name"
                id="firstname"
                className="form-control"
                value={formData.first_name}
              />
              <label htmlFor="firstname">First Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Last Name"
                required
                type="text"
                name="last_name"
                id="lastname"
                className="form-control"
                value={formData.last_name}
              />
              <label htmlFor="lastname">Last Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                placeholder="Employee ID"
                required
                type="text"
                name="employee_id"
                id="employeeId"
                className="form-control"
                value={formData.employee_id}
              />
              <label htmlFor="employeeId">Employee ID</label>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default SalespersonForm;
