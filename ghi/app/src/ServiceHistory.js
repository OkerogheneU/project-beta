import React, { useState } from 'react';

function ServiceHistory() {
  const [vin, setVin] = useState('');
  const [history, setHistory] = useState([]);
  const [error, setError] = useState('');

  const handleSearch = (event) => {
    event.preventDefault();
    fetch(`http://localhost:8080/api/appointments?vin=${vin}`)
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to fetch history');
        }
        return response.json();
      })
      .then(data => {
        setHistory(data);
        setError('');
      })
      .catch(error => {
        console.error('Error:', error);
        setError(error.message);
      });
  };


  return (
    <div>
      <form onSubmit={handleSearch}>
        <label htmlFor="vinSearch">Search VIN:</label>
        <input
          id="vinSearch"
          type="text"
          value={vin}
          onChange={e => setVin(e.target.value)}
        />
        <button type="submit">Search</button>
      </form>
      {error && <div className="error">Error: {error}</div>}
      <h2>Service History for VIN: {vin}</h2>
      <ul>
        {history.length > 0 ? (
          history.map(app => (
            <li key={app.id}>
              {app.date_time} - {app.customer} ({app.vin}) - {app.reason} - Status: {app.status}
            </li>
          ))
        ) : (
          <li>No service history found</li>
        )}
      </ul>
    </div>
  );
}

export default ServiceHistory;
