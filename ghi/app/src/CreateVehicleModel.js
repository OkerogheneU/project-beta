import React, { useState } from 'react';

const CreateVehicleModel = () => {
  const [newVehicleModel, setNewVehicleModel] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: '',
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setNewVehicleModel((prevModel) => ({
      ...prevModel,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch('http://localhost:8100/api/models/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newVehicleModel),
      });

      if (response.ok) {
        // Clear the form fields by resetting the state
        setNewVehicleModel({
          name: '',
          picture_url: '',
          manufacturer_id: '',
        });

        // Show a dialog saying "Vehicle Added!"
        alert('Vehicle Added!');

        // Optionally, handle the response data or redirect the user
        // const data = await response.json();
        // console.log('Vehicle model created:', data);
      } else {
        // Handle HTTP errors
        console.error('Failed to create vehicle model, response:', response);
      }
    } catch (error) {
      console.error('Error creating vehicle model:', error);
    }
  };

  return (
    <div>
      <h2>Create a Vehicle Model</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Vehicle Name:</label>
          <input
            type="text"
            id="name"
            name="name"
            value={newVehicleModel.name}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="manufacturer_id">Manufacturer ID:</label>
          <input
            type="number"
            id="manufacturer_id"
            name="manufacturer_id"
            value={newVehicleModel.manufacturer_id}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="picture_url">Picture URL:</label>
          <input
            type="text"
            id="picture_url"
            name="picture_url"
            value={newVehicleModel.picture_url}
            onChange={handleInputChange}
          />
        </div>
        <button type="submit">Create Vehicle Model</button>
      </form>
    </div>
  );
};

export default CreateVehicleModel;
