# CarCar

The project is CarCar, an application for managing aspects of an automobile dealership—specifically its inventory, service center, and sales.

Team:

- Person 1 - Carlos Diaz - Auto Services
- Person 2 - Gift Ugbeyide - Auto Sales

## Design

CarCar consists of three interconnected microservices:

Inventory
Sales
Services

## How to Run this App

- Put instructions to build and run this app here

1. Log into gitlab, fork the project using ssh or https, invite your pair using the manage, members and invite members button options in the "Maintainer" role from the dropdown options.

Also want to ensure that Docker desktop is up and running using the following commands:

1. docker volume create beta-data: This file is to create a volume called beta-data.

2. docker-compose build: This is a command used in Docker to create or update the Docker images defined in a docker-compose.yml file.
3. docker-compose up: This is a command used in Docker to start and run the services defined in your docker-compose.yml

4. Clone the project(Project Beta) using your terminal, and use code editor(VStudio) of choice.

5. Use API (Insomnia to List(Get), Delete(Del) and Create Salesperson(Post), Customers and Sales using the provided end point.)

6. View the project in the browser: http://localhost:3000/

## Service microservice

Hello and welcome to the wonderful world of service!!
As explained above, the service microservice is an extension of the dealership that looks to provide service repairs for your vehicle.
We keep track of three things:

1. Our friendly technician staff
2. Service Appointments
3. AutomobileVO (vin)

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

The models for Salespeople, Customers, Sales, and Automobiles (represented by AutomobileVO) in the Automobile Sales microservice work together to create a strong foundation for organizing data related to sales. The Salesperson model captures key details about individuals involved in sales, while the Customer model comprehensively stores customer information. The Sale model serves as a central entity for recording sales transactions, referencing the AutomobileVO model for automobile details. The AutomobileVO model, with attributes like VIN and sold status, plays a pivotal role in representing automobile data.

These models seamlessly integrate with the Inventory microservice through the Automobile Poller. Also fetching and updating the AutomobileVO every 60 seconds with updated VINs from the Inventory service.

## Diagram

- Put diagram here
![alt text](excalidraw.png)
![alt text](image.png)


## API Documentation
![alt text](<API Documentation.png>)

![alt text](<API Documentation 2.png>)

![alt text](capture1.PNG)

![alt text](Capture2.PNG)

![alt text](capture3.PNG)
### URLs and Ports

Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

Manufacturers.

1. List manufacturers | GET | http://localhost:8100/api/manufacturers/
2. Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
3. Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
4. Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
5. Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/


Creating and updating a manufacturer requires only the manufacturer's name.

{
  "name": "Chrysler"
}
The return value of creating, getting, and updating a single manufacturer is its name, href, and id.

{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}
The list of manufacturers is a dictionary with the key "manufacturers" set to a list of manufacturers.

{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}

Automobiles.

1. List automobiles | GET | http://localhost:8100/api/automobiles/
2. Create an automobile | POST | http://localhost:8100/api/automobiles/
3. Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/ (query an automobile by its VIN)
4. Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
5. Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/

You can create an automobile with its color, year, VIN, and the id of the vehicle model.

{
    "color": "red",
    "year": 2012,
    "vin": "1C3CC5FB2AN120174",
    "model_id": 1
}

Return Value of Creating an Automobile:

```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}

To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

Return Value:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}



```
You can update the color and/or year of an automobile (SEND THIS JSON BODY):
```

{
    "color": "red",
    "year": 2012
}

```
Getting a list of Automobile Return Value:
```
   {
			"href": "/api/automobiles/5R457YFESG67LVC/",
			"id": 6,
			"color": "Green",
			"year": 2021,
			"vin": "5R457YFESG67LVC",
			"model": {
				"href": "/api/models/10/",
				"id": 10,
				"name": "Subaru brz",
				"picture_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBoLSsGWCDsd7nHhNZqKNGbqxLBl8BRdSKIg&usqp=CAU",
				"manufacturer": {
					"href": "/api/manufacturers/8/",
					"id": 8,
					"name": "Subaru"
				}
			},
			"sold": false
		},
```



 Vehicle Models.

 List vehicle models | GET | http://localhost:8100/api/models/
 Create a vehicle model | POST | http://localhost:8100/api/models/
 Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
 Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
 Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

 Creating a vehicle model requires the model name, a URL of an image, and the id of the manufacturer.

{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
Updating a vehicle model can take the name and/or the picture URL. It is not possible to update a vehicle model's manufacturer.

{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
Getting the detail of a vehicle model, or the return value from creating or updating a vehicle model, returns the model's information and the manufacturer's information.

{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
Getting a list of vehicle models returns a list of the detail information with the key "models".

{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}

- Put URLs and ports for sales here

Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Salespeople:

1. List salespeople | GET | http://localhost:8090/api/salespeople/
2. Salesperson details | GET | http://localhost:8090/api/salesperson/id/
3. Create a salesperson | POST | http://localhost:8090/api/salespeople/
4. Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/




To create a salesperson (SEND THIS JSON BODY):
```

{
    "first_name": "Him",
    "last_name": "Sales",
    "employee_id": "008"
}

Return Value of creating a salesperson:

```
{
    {

		"id": 13,
		"first_name": "Him",
		"last_name": "Sales",
		"employee_id": "008"
	}
}

```

List all salespeople Return Value:
```{
	"salespeople": [
		{
			"id": 2,
			"first_name": "Gift",
			"last_name": "Gift",
			"employee_id": "001"
		}
	]
}
```

### Customers:

| Action | Method | URL

1. List customers | GET | http://localhost:8090/api/customers/
2. Create a customer | POST | http://localhost:8090/api/customers/
3. Show a specific customer | GET | http://localhost:8090/api/customers/id/

To create a Customer (SEND THIS JSON BODY):
```
{
  "first_name": "Deeysd",
  "last_name": "Jin",
  "address": "123 corbin street, Wincity",
  "phone_number": "881-151-6261"
}
```
Return Value of Creating a Customer:
```
{
    		{
			 "/api/customers/10/",
			"id": 10,
			"first_name": "Deeysd",
			"last_name": "Jin",
			"address": "123 corbin street, Wincity",
			"phone_number": "881-151-6261"
		}

}

```
Return value of Listing all Customers:
```
{
	"customers": [
		{
				{

			"id": 1,
			"first_name": "Justin",
			"last_name": "Timberlake",
			"address": "123 Relias St, City",
			"phone_number": "857-945-4721"
		},
		},
		{
			"id": 7,
			"first_name": "Rohit",
			"last_name": "Kaira",
			"address": "123 Corbin, Cityville",
			"phone_number": "281-815-9268"
		}
	]
}
```
### Salesrecords:
- the id value to show a salesperson's salesrecord is the **"id" value tied to a salesperson.**

| Action | Method | URL
| ----------- | ----------- | ----------- |
1. List all salesrecords | GET | http://localhost:8090/api/salesrecords/
2. Create a new sale | POST | http://localhost:8090/api/salesrecords/
3. Show salesperson's salesrecords | GET | http://localhost:8090/api/salesrecords/id/
List all Salesrecords Return Value:
```
{
	"sales": [
		{
			"id": 1,
			"price": 111000,
			"vin": {
				"vin": "111"
			},
			"salesperson": {
				"id": 1,
				"name": "Liz",
				"employee_number": 1
			},
			"customer": {
				"name": "Martha Stewart",
				"address": "1313 Baker Street",
				"phone_number": "980720890"
			}
		}
	]
}
```
Create a New Sale (SEND THIS JSON BODY):
```
{
	"salesperson": "Liz",
	"customer": "John Johns",
	"vin": "888",
	"price": 40000
}
```
Return Value of Creating a New Sale:
```
{
	"id": 4,
	"price": 40000,
	"vin": {
		"vin": "888"
	},
	"salesperson": {
		"id": 1,
		"name": "Liz",
		"employee_number": 1
	},
	"customer": {
		"id",
		"name": "John Johns",
		"address": "1212 Ocean Street",
		"phone_number": "9804357878"
	}
}
```


- Put URLs and ports for services here

### Inventory API (Optional)

- Put Inventory API documentation here. This is optional if you have time, otherwise prioritize the other services.

### Service API



### Sales API

- Put Sales API documentation here

## Value Objects

- Services API checks for appointments(?)
