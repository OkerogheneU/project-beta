from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Sale, AutomobileVO, Salesperson, Customer
import json
from .models import AutomobileVO



class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
    ]
    encoder = {"automobile": AutomobileVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        response_data = {"data": []}

        for salesperson in salespeople:
            salesperson_data = {
                "href": f"/api/salespeople/{salesperson.id}/",
                "id": salesperson.id,
                "first_name": salesperson.first_name,
                "last_name": salesperson.last_name,
                "employee_id": salesperson.employee_id,
            }
            response_data["data"].append(salesperson_data)

        return JsonResponse(response_data, safe=False)

    elif request.method == "POST":
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({"error": "Invalid JSON in request body"}, status=400)

        salesperson = Salesperson.objects.create(**content)

        response_data = {
            "data": {
                "href": f"/api/salespeople/{salesperson.id}/",
                "id": salesperson.id,
                "first_name": salesperson.first_name,
                "last_name": salesperson.last_name,
                "employee_id": salesperson.employee_id,
            }
        }

        return JsonResponse(response_data, safe=False)



@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()

        customer_list = [
            {
                "href": f"/api/customers/{customer.id}/",
                "id": customer.id,
                "first_name": customer.first_name,
                "last_name": customer.last_name,
                "address": customer.address,
                "phone_number": customer.phone_number,
            }
            for customer in customers
        ]

        return JsonResponse({"data": customer_list}, safe=False)

    elif request.method == "POST":
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({"error": "Invalid JSON in request body"}, status=400)

        customer = Customer.objects.create(**content)

        customer_data = {
            "href": f"/api/customers/{customer.id}/",
            "id": customer.id,
            "first_name": customer.first_name,
            "last_name": customer.last_name,
            "address": customer.address,
            "phone_number": customer.phone_number,
        }

        return JsonResponse({"data": [customer_data]}, safe=False)

@require_http_methods(["DELETE", "GET"])
def api_show_customer(request, pk):
    try:
        customer = Customer.objects.get(id=pk)
    except Customer.DoesNotExist:
        return JsonResponse({"error": "Customer does not exist"}, status=404)

    if request.method == "GET":
        customer_data = {
            "href": f"/api/customers/{customer.id}/",
            "id": customer.id,
            "first_name": customer.first_name,
            "last_name": customer.last_name,
            "address": customer.address,
            "phone_number": customer.phone_number,
        }

        return JsonResponse({"data": customer_data}, safe=False)

    elif request.method == "DELETE":
        customer.delete()
        return JsonResponse({"message": "Customer deleted successfully"}, safe=False)


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({"data": SaleEncoder().encode(sales)}, safe=False)
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({"error": "Invalid JSON in request body"}, status=400)

        automobile_data = content.get("automobile")
        vin = automobile_data.get("vin") if automobile_data else None

        print("Received VIN:", vin)

        try:
            automobile = AutomobileVO.objects.get(vin=vin)
        except AutomobileVO.DoesNotExist:
            print("Automobile not found for VIN:", vin)
            return JsonResponse({"error": f"Automobile not found for VIN: {vin}"}, status=400)

        content["automobile"] = automobile
        sale = Sale.objects.create(**content)
        sale_data = SaleEncoder().encode(sale)

        return JsonResponse({"data": sale_data}, safe=False)







@require_http_methods(["DELETE", "GET"])
def api_show_sale(request, pk):
    try:
        sale = Sale.objects.get(id=pk)
    except Sale.DoesNotExist:
        return JsonResponse({"error": "Sale does not exist"}, status=404)

    if request.method == "GET":
        return JsonResponse({"data": SaleEncoder().encode(sale)}, safe=False)
    elif request.method == "DELETE":
        sale.delete()
        return JsonResponse({"message": "Sale deleted successfully"}, safe=False)

@require_http_methods(["DELETE"])
def api_delete_salesperson(request, pk):
    try:
        salesperson = Salesperson.objects.get(pk=pk)
    except Salesperson.DoesNotExist:
        return JsonResponse({"error": "Salesperson not found"}, status=404)

    salesperson.delete()
    return JsonResponse({}, status=204)


@require_http_methods(["DELETE", "GET"])
def api_show_salesperson(request, pk):
    try:
        salesperson = Salesperson.objects.get(id=pk)
    except Salesperson.DoesNotExist:
        return JsonResponse({"error": "Salesperson does not exist"}, status=404)

    if request.method == "GET":
        return JsonResponse({"data": SalespersonEncoder().encode(salesperson)}, safe=False)
    elif request.method == "DELETE":
        salesperson.delete()
        return JsonResponse({"message": "Salesperson deleted successfully"}, safe=False)


@require_http_methods(["DELETE"])
def api_delete_customer(request, pk):
    try:
        customer = Customer.objects.get(id=pk)
    except Customer.DoesNotExist:
        return JsonResponse({"error": "Customer not found"}, status=404)

    customer.delete()
    return JsonResponse({}, status=204)
