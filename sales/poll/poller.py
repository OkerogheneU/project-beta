import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here. Ignore vs-code error hinting
# from sales_rest.models import Something

from sales_rest.models import AutomobileVO

def get_updated_vins():
    # Assume you have an API endpoint for the Inventory service that provides updated VINs
    response = requests.get("http://inventory-service:8000/api/automobiles/")
    content = json.loads(response.content)
    return content.get("updated_vins", [])

def update_automobile_vo(self):
        updated_vins = self.inventory_service.get_updated_vins()
        self.automobile_vo = [AutomobileVO(vin) for vin in updated_vins]
        print(f"Updated AutomobileVO: {[car.vin for car in self.automobile_vo]}")


def poll():
    while True:
        print('Sales poller polling for data')
        try:
            # Write your polling logic, here
            # Do not copy entire file

            get_updated_vins
        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(60)


if __name__ == "__main__":
    poll()
