"""service_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from service_rest import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/technicians/', views.technician_list_create, name='technician-list-create'),
    path('api/technicians/<int:id>/', views.technician_delete, name='technician-delete'),
    path('api/appointments/', views.appointment_list_create, name='appointment-list-create'),
    path('api/appointments/<int:id>/', views.appointment_delete, name='appointment-delete'),
    path('api/appointments/<int:id>/status/', views.appointment_update_status, name='appointment-update-status'),
    path('api/appointments', views.get_appointments, name='get_appointments'),
]
