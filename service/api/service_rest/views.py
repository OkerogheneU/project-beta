from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse, HttpResponse
import json
from .models import Technician, Appointment


@require_http_methods(["GET", "POST"])
def technician_list_create(request):
    if request.method == 'GET':
        technicians = Technician.objects.all()
        technicians_list = list(technicians.values())
        return JsonResponse(technicians_list, safe=False)
    elif request.method == 'POST':
        data = json.loads(request.body)
        technician = Technician.objects.create(**data)
        return JsonResponse({"id": technician.id}, status=201)


def technician_delete(request, id):
    try:
        technician = Technician.objects.get(id=id)
        technician.delete()
        return HttpResponse(status=204)
    except Technician.DoesNotExist:
        return JsonResponse({'error': 'Technician not found.'}, status=404)


def appointment_list_create(request):
    if request.method == 'GET':
        appointments = Appointment.objects.all()
        appointments_list = [
            {
                'id': appointment.id,
                'date_time': appointment.date_time,
                'reason': appointment.reason,
                'status': appointment.status,
                'vin': appointment.vin,
                'customer': appointment.customer,
                'technician': appointment.technician.id if appointment.technician else None,

            }
            for appointment in appointments
        ]
        return JsonResponse(appointments_list, safe=False)
    elif request.method == 'POST':
        data = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=data['technician'])
            appointment = Appointment.objects.create(
                date_time=data['date_time'],
                reason=data['reason'],
                status=data['status'],
                vin=data['vin'],
                customer=data['customer'],
                technician=technician
            )
            return JsonResponse({"id": appointment.id}, status=201)
        except Technician.DoesNotExist:
            return JsonResponse({'error': 'Technician not found.'}, status=404)


def appointment_delete(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.delete()
        return HttpResponse(status=204)
    except Appointment.DoesNotExist:
        return JsonResponse({'error': 'Appointment not found.'}, status=404)



def appointment_update_status(request, id):
    try:
        data = json.loads(request.body)
        status = data.get('status')
        if status not in ['canceled', 'finished']:
            return JsonResponse({'error': 'Invalid status update.'}, status=400)

        appointment = Appointment.objects.get(id=id)
        appointment.status = status
        appointment.save()
        return HttpResponse(status=204)
    except Appointment.DoesNotExist:
        return JsonResponse({'error': 'Appointment not found.'}, status=404)
    except json.JSONDecodeError:
        return JsonResponse({'error': 'Invalid JSON.'}, status=400)

def get_appointments(request):
    vin = request.GET.get('vin')
    appointments = Appointment.objects.filter(vin=vin)
    appointments_data = [
        {
            'id': appointment.id,
            'date_time': appointment.date_time,
            'reason': appointment.reason,
            'status': appointment.status,
            'vin': appointment.vin,
            'customer': appointment.customer,
            'technician': appointment.technician
        }
        for appointment in appointments
    ]
    return JsonResponse(appointments_data, safe=False)
