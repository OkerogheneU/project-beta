import django
import os
import sys
import time
import requests

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import AutomobileVO

def get_updated_vins():
    response = requests.get("http://inventory-api:8000/api/vins/")
    if response.status_code == 200:
        content = json.loads(response.content)
        return content.get("automobiles", [])
    else:
        return []

def update_automobile_vo(updated_vins):
    for automobile in updated_vins:
        vin = automobile.get("vin_key", "")
        AutomobileVO.objects.update_or_create(
            vin=vin,
            defaults={"sold": True}
        )

def poll():
    while True:
        print('Service poller polling for data')
        try:
            updated_vins = get_updated_vins()
            if updated_vins:
                update_automobile_vo(updated_vins)
            else:
                print("No new data to update.")
        except Exception as e:
            print(f"An error occurred: {e}", file=sys.stderr)

        time.sleep(60)

if __name__ == "__main__":
    poll()
